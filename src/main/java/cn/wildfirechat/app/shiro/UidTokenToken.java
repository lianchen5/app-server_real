package cn.wildfirechat.app.shiro;

import org.apache.shiro.authc.AuthenticationToken;

public class UidTokenToken implements AuthenticationToken {
    final private String uid;
    final private String token;

    public UidTokenToken(String uid, String token) {
        this.uid = uid;
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return uid;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
