package cn.wildfirechat.app;

import cn.wildfirechat.app.jpa.UserPhotoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class LoginResponseVO {
    private Long uid;
    private String token;
    private String nickName;
    private String phone;
    private String mail;
    private Integer mailCert;
    private String portrait;
    private Integer sex;
    private Integer level;
    private Long exp;
    private String constellation;
    private String sentiment;
    private String whatsup;
    private Integer tourist;
    private String bg;

    private List<UserPhotoEntity> userPhotoVOList;

    public LoginResponseVO(Long uid, String token, String nickName, String phone, String mail, Integer mailCert, String portrait, Integer sex, Integer level, Long exp, String constellation, String sentiment, String whatsup, Integer tourist, String bg) {
        this.uid = uid;
        this.token = token;
        this.nickName = nickName;
        this.phone = phone;
        this.mail = mail;
        this.mailCert = mailCert;
        this.portrait = portrait;
        this.sex = sex;
        this.level = level;
        this.exp = exp;
        this.constellation = constellation;
        this.sentiment = sentiment;
        this.whatsup = whatsup;
        this.tourist = tourist;
        this.bg = bg;
    }
}
