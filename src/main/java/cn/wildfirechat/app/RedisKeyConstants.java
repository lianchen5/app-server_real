package cn.wildfirechat.app;

public class RedisKeyConstants {
    public final static String USER_TOKEN_KEY = "user:token:";
    public final static String HTTP_NONCE_KEY = "nonce:";
    public final static String MAIL_CERT_KEY = "mail:cert:";
    public final static String INDEX_GAME_RECOMMEND_LIST_KEY = "game:index:rec_list";
    public final static String USER_INFO = "user:info:";
    public final static String USER_PHONE_VCODE_KEY = "user:phone:vcode:";
}
